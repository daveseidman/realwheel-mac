import * as tf from '@tensorflow/tfjs';
import * as tfvis from '@tensorflow/tfjs-vis';

import './index.scss';

const dataRawDisplay = document.querySelector('.data-body');
const clearCurrentButton = document.querySelector('#clearCurrent');
const trainButton = document.querySelector('#train');
const capturePad = document.querySelector('.capture-body');
const saveTrainingButton = document.querySelector('#saveData');
const loadTrainingButton = document.querySelector('#loadData');
const saveModelButton = document.querySelector('#saveModel');
const loadModelButton = document.querySelector('#loadModel');
const predictButton = document.querySelector('#predict');
const windowSizeInput = document.querySelector('#windowsize');
const predictionAmount = document.querySelector('.prediction-amount');
const captureXCheckbox = document.querySelector('#capture-x');
const captureYCheckbox = document.querySelector('#capture-y');
const realCaptured = document.querySelector('#realCaptured');
const fakeCaptured = document.querySelector('#fakeCaptured');
const app = document.querySelector('.app');

let count = 0;
let model = null;
let training = false;
let predicting = false;
let prevDelta = { deltaX: 0, deltaY: 0 };
const allDeltas = [];
const allOffsets = [];
// window.allDeltas = allDeltas;
// window.allOffsets = allOffsets;
let windowSize = 20;
let captureX = true;
let captureY = true;
let prevTimeStamp = Date.now();
let predictingDelayed = false;
const realSequences = [];
const fakeSequences = [];
const realLabels = [];
const fakeLabels = [];

let afterWheelEventTimeout = null;
let wheelTimedOut = true;

// const worker = new Worker('worker.js');
// worker.onmessage = (e) => {
//   console.log('Message received from worker', e.data);
// };

const updateUI = () => {
  trainButton.disabled = realSequences.length === 0 || fakeSequences.length === 0;
  predictButton.disabled = training || !model;
  predictButton.innerText = predicting ? 'Stop Predicting' : 'Start Predicting';
  realCaptured.innerText = realSequences.length;
  fakeCaptured.innerText = fakeSequences.length;
};

const pad = (number, length) => {
  let string = number.toString();
  const originalLength = string.length;
  for (let i = 0; i < length - originalLength; i += 1) {
    string = `&nbsp${string}`;
  }
  // while (string.length < length) string = `&nbsp;${string}`;
  return string;
};

const addData = (offsetX, delay, real) => {
  const display = document.createElement('div');
  display.className = real ? 'real' : 'fake';
  display.innerHTML = `[${pad(offsetX, 2)}, ${pad(offsetX, 2)}, ${pad(delay.toFixed(2), 5)}], ${real ? '1' : '0'}`;
  dataRawDisplay.appendChild(display);
  dataRawDisplay.scrollTop = -dataRawDisplay.scrollHeight;
};

const getSequences = () => {
  realSequences.length = 0;
  realLabels.length = 0;
  fakeSequences.length = 0;
  fakeLabels.length = 0;

  for (let i = 0; i < allOffsets.length - windowSize; i += 1) {
    const all = allOffsets.slice(i, i + windowSize);
    const first = allOffsets[i];
    const rest = allOffsets.slice(i + 1, i + windowSize);
    // first event was real, the rest were fake, this represents the trackpad taking over and issuing fake events
    const fake = first.real && rest.every((item) => !item.real);
    const real = all.every((item) => item.real);

    if (real) {
      realSequences.push(all.map((item) => ([item.offsetX, item.offsetY, item.delay])));
      realLabels.push(1);
    }
    if (fake) {
      fakeSequences.push(all.map((item) => ([item.offsetX, item.offsetY, item.delay])));
      fakeLabels.push(0);
    }

    if (real || fake) {
      dataRawDisplay.children[i].classList.add('start');
      dataRawDisplay.children[i + windowSize].classList.add('end');
      i += windowSize;
    }
  }

  updateUI();
};

const afterWheelEvent = () => {
  wheelTimedOut = true;
  predictionAmount.style.left = '50%';
  getSequences();
};

const handleWheelEvent = ({ deltaX, deltaY, shiftKey, timeStamp }) => {
  // worker.postMessage({ deltaX });

  let delay = allDeltas.length ? timeStamp - prevTimeStamp : 0;
  if (delay > 100) delay = 0;
  prevTimeStamp = timeStamp;
  allDeltas.push({ deltaX, deltaY, real: shiftKey, delay, time: timeStamp });
  const offsetX = Math.abs(deltaX - prevDelta.deltaX);
  const offsetY = Math.abs(deltaY - prevDelta.deltaY);
  const real = shiftKey;
  if (!predicting) predictionAmount.style.left = `${real ? 100 : 0}%`;
  allOffsets.push({ offsetX, offsetY, real, delay, time: timeStamp, count });
  prevDelta = { deltaX, deltaY };

  // TODO: add a debounced event here to reset the timer and prediction amount to 0.5
  wheelTimedOut = false;
  if (afterWheelEventTimeout) clearTimeout(afterWheelEventTimeout);
  afterWheelEventTimeout = setTimeout(afterWheelEvent, 1000);
  addData(offsetX, delay, real);

  if (predicting) {
    const { length } = allOffsets;
    const sequence = allOffsets.slice(length - windowSize - 1, length - 1).map((item) => ([item.offsetX, item.offsetY, item.delay]));
    // console.log(sequence);
    if (!predictingDelayed) {
      const predictions = model.predict(new tf.tensor3d([sequence]));
      const predictionsValues = predictions.dataSync();
      predictionAmount.style.left = `${predictionsValues[0] * 100}%`;
      predictingDelayed = true;
      setTimeout(() => { predictingDelayed = false; }, 30);
    }
  }

  count += 1;
};

const clearCurrentData = (e) => {
  allDeltas.length = 0;
  allOffsets.length = 0;
  dataRawDisplay.innerHTML = '';
  realSequences.length = 0;
  fakeSequences.length = 0;
  updateUI();
  e.target.blur();
};

// const clearSavedData = (e) => {
//   localStorage.setItem('allOffsets', '[]');
//   localStorage.setItem('allDeltas', '[]');
//   e.target.blur();
// };

const startTraining = () => {
  app.classList.add('disabled');
  const sequences = realSequences.concat(fakeSequences);
  const labels = realLabels.concat(fakeLabels);
  const epochs = 20;

  setTimeout(() => {
    if (!model) {
      model = tf.sequential();
      model.add(tf.layers.lstm({ inputShape: [windowSize, captureX && captureY ? 3 : 2], units: 64, returnSequences: true }));
      // model.add(tf.layers.lstm({ inputShape: [windowSize, 3], units: 64, returnSequences: true }));
      model.add(tf.layers.lstm({ units: 10 }));
      model.add(tf.layers.dense({ units: 1, activation: 'sigmoid' }));
      model.compile({ optimizer: 'adam', loss: 'binaryCrossentropy', metrics: ['accuracy'] });
    }

    training = true;

    const callbacks = [tfvis.show.fitCallbacks({ name: 'Training Performance' }, ['loss', 'acc'])];

    tfvis.show.fitCallbacks(document.getElementById('tensorboard'));

    model.fit(tf.tensor3d(sequences), tf.tensor1d(labels), { epochs, batchSize: 32, callbacks })
      .then((info) => {
        console.log('Training complete:', info);
        training = false;
        predicting = true;
        // model.save('localstorage://wheelevents');
        app.classList.remove('disabled');
        updateUI();
      });
  }, 100);
};

const togglePredicting = () => {
  predicting = !predicting;
  updateUI();
};

const saveTrainingData = () => {
  localStorage.setItem('allOffsets', JSON.stringify(allOffsets));
  localStorage.setItem('allDeltas', JSON.stringify(allDeltas));
};

const loadTrainingData = () => {
  allOffsets.length = 0;
  allDeltas.length = 0;

  allOffsets.splice(0, allOffsets.length, ...JSON.parse(localStorage.getItem('allOffsets')));
  allDeltas.splice(0, allDeltas.length, ...JSON.parse(localStorage.getItem('allDeltas')));

  dataRawDisplay.innerHTML = '';
  allOffsets.forEach((offset) => {
    const { real, offsetX, offsetY, delay } = offset;
    addData(offsetX, offsetY, delay, real);
  });
  getSequences();
};

const saveModel = () => {
  model.save('localstorage://model').then((res) => {
    console.log(res);
  });
};

const loadModel = () => {
  // worker.postMessage({ action: 'loadModel' });
  tf.loadLayersModel('localstorage://model', { strict: false }).then((res) => {
    console.log(res);
    model = res;
    predicting = true;
    updateUI();
  });
};

capturePad.addEventListener('wheel', handleWheelEvent);
clearCurrentButton.addEventListener('click', clearCurrentData);
// clearSavedButton.addEventListener('click', clearSavedData);
trainButton.addEventListener('click', startTraining);
predictButton.addEventListener('click', togglePredicting);
saveTrainingButton.addEventListener('click', saveTrainingData);
loadTrainingButton.addEventListener('click', loadTrainingData);
saveModelButton.addEventListener('click', saveModel);
loadModelButton.addEventListener('click', loadModel);
windowSizeInput.addEventListener('change', (e) => { windowSize = parseInt(e.target.value, 10); });
captureXCheckbox.addEventListener('change', (e) => { captureX = e.target.checked; captureYCheckbox.disabled = !captureX; });
captureYCheckbox.addEventListener('change', (e) => { captureY = e.target.checked; captureXCheckbox.disabled = !captureY; });
updateUI();

# RealWheel

The Mac trackpad (and possibly Magic Mouse) issue synthetic wheel events inside the browser. This is how they accomplish the inertial smoothing effect on scrolling but makes it difficult for a developer to know when the user has actually stopped touching their trackpad. This repo uses TensorFlow JS to accurately differentiate between real user-initiated wheel events and the synthetic ones that are automatically issued by the device or operating system.

## Remaining TODO:

- Take prediction off main thread, use webworker
- Instead of \<div\> s for each training data, use text for the entire block with /n's as it's beginning to make the page less responsive.
- Play with training values, layers, etc.
- Begin issuing "Takeover" (or maybe pointerup) events when we think Mac OS' synthetic events have begin
- Modularize this so that it can be imported and used on any page
